#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>

#define MAX 6

int vertexCount = 0;

struct vertex {
	char data;
	bool visited;
} *graph[MAX];

int adjMatrix[MAX][MAX];

int stack[MAX];
int top = -1;

void push(int data) {
	stack[++top] = data;
}

int pop() {
	return stack[top--];
}

int peek() {
	return stack[top];
}

bool isStackEmpty() {
	return top == -1;
}

//add vertex to the vertex list
void addVertex(char data) {
	struct vertex *new = (struct vertex *)malloc(sizeof(struct vertex));
	new -> data = data;
	new -> visited = false;
	graph[vertexCount] = new;
	vertexCount++;
}

//add edge to edge array
void addEdge(int start, int end) {
	adjMatrix[start][end] = 1;
	adjMatrix[end][start] = 1;
}

//to return adjacent vertex
int adjVertex(int vertexGet) {
	int i;
	for(i = 0; i < vertexCount; i++) {
		if(adjMatrix[vertexGet][i] == 1 && graph[i] -> visited == false)
			return i;
	}
	return -1;
}

//to display vertex value
void displayVertex(int pos) {
	printf("/n%c", graph[pos] -> data);
}

void dfs() {
	int i;
	int unvisisted;
	printf("\n\n");
	graph[0] -> visited = true;
	displayVertex(0);
	push(0);
	while(!isStackEmpty()){
		int unvisisted = adjVertex(peek());
	
		if(unvisisted == -1) 
			pop();

		else {
			graph[unvisisted] -> visited = true;
			displayVertex(unvisisted);
			push(unvisisted);
		}
	}
	printf("\n\n");
	for(i = 0; i < vertexCount; i++)
		graph[i] -> visited = false;
}

void show() {
	int i;
	printf("\n\n");
	for(i = 0; i < vertexCount; i++)
		printf("Edge position of '%c' is %d\n", graph[i] -> data, i);
	printf("\n\n");
}

void main() {
	int opt;
	char data;
	int edge_1, edge_2;
	int i, j;
	for(i = 0; i < MAX; i++)
		for(j = 0; j < MAX; j++)
			adjMatrix[i][j] = 0;

	do {
		printf("\n1.Add vertex\n2.Create edge\n3.Traversal\n4.Exit\nChoose option : ");
		scanf("%d", &opt);
		switch(opt) {

			case 1: printf("\nEnter data to be added to vertex : ");
					scanf("%c", &data);
					addVertex(data);
					break;

			case 2:	show(); 
					printf("\nEnter edge starting : ");
					scanf("%d", &edge_1);
					printf("\nEnter edge ending edge : ");
					scanf("%d", &edge_2);
					if(vertexCount - 1 < edge_1 || vertexCount - 1 < edge_2)
						printf("\nThere is no vertex !!\n");
					else
						addEdge(edge_1, edge_2);
					break;
			
			case 3: dfs();
					break;

			default: printf("\nInvalid option try again !! ");	
		}
	} while(opt != 4);
}